class VendorQuestion < ActiveRecord::Base

  attr_accessible :options, :question, :section, :question_type,:position

  validates_presence_of :question,:section,:question_type
  validates_presence_of :options, :if => Proc.new {|q| q.multiple_answer? || q.multiple_choice?}


  SECTIONS = {"General Information" => "General","Logistics" => "Logistics"}
  TYPES = {"Multiple Choice" => "multiple_choice","Text" => "text","Multiple Answer" => "multiple_answer"}

  has_many :answers, :dependent => :destroy
  has_many :vendors, :through => :answers

  default_scope order('position ASC')
  scope :general_information, where(:section => VendorQuestion::SECTIONS["General Information"])
  scope :logistics, where(:section => VendorQuestion::SECTIONS["Logistics"])

  acts_as_list :scope => 'section = \'#{section}\''

  after_create :create_answers, :set_position

  def multiple_choice?
    self.question_type.eql?("multiple_choice")
  end

  def multiple_answer?
    self.question_type.eql?("multiple_answer")
  end

  def options_array
    self.options.split(",")
  end

  def create_answers
   Vendor.all.each do |v|
      Answer.create({:vendor_question_id => self.id,:vendor_id => v.id})
    end
  end

  def set_position
    self.move_to_bottom
  end

end
