class UploadedFile < ActiveRecord::Base
  # attr_accessible :title, :body

  has_attached_file :blob, styles:  lambda { |a| a.instance.blob_content_type =~ %r(image) ? {:small => "x200>", :medium => "x300>", :large => "x400>"}  : {} }

  attr_accessible :blob, :uploaded_by

  do_not_validate_attachment_file_type :blob

  belongs_to :uploaded_by, :class_name => "User", :foreign_key => :uploaded_by_id
  has_one :uploaded_file_on_item

  scope :vendor_files, where(:is_for_vendor => true)
  default_scope where(:deleted_at => nil)
end
