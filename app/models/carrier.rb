class Carrier < ActiveRecord::Base
  belongs_to :user
  attr_accessible :company_name,:deleted_at
  validates_presence_of :company_name

  has_many :contacts,:as => :item

  self.per_page = 20
end
