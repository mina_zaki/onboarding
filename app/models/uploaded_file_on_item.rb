class UploadedFileOnItem < ActiveRecord::Base
  belongs_to :item, :polymorphic => true
  belongs_to :uploaded_file
end
