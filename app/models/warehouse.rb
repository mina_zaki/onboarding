class Warehouse < ActiveRecord::Base
  attr_accessible :city, :country, :name, :phone, :postcode, :street,:vendor,:vendor_id,:deleted_at,:ship_label_generation,:pickup_trucks,:remarks,:height_of_ramps

  validates_presence_of :city, :country, :name, :phone, :postcode, :street

  belongs_to :vendor

  default_scope where(:deleted_at => nil)

  def ship_label_generation=(value)
    self[:ship_label_generation] = value.reject(&:blank?).join(",")
  end

  def ship_label_generation
    self[:ship_label_generation] ? self[:ship_label_generation].split(',') : []
  end

  def pickup_trucks=(value)
    self[:pickup_trucks] = value.reject(&:blank?).join(",")
  end

  def pickup_trucks
    self[:pickup_trucks] ? self[:pickup_trucks].split(',') : []
  end

  def remarks=(value)
     self[:remarks] = value.reject(&:blank?).join(",")
  end

  def remarks
    self[:remarks] ? self[:remarks].split(',') : []
  end

  def other_value
    difference = self.ship_label_generation -  WarehouseChoices['ship_label_generation']
    if difference.present?
      return difference
    end
  end

end
