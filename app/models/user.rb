class User < ActiveRecord::Base

  devise :invitable, :database_authenticatable, :encryptable,
         :recoverable, :rememberable, :trackable, :deactivatable
  devise :invitable, :encryptor => :sha512

  attr_accessible :email, :name,:password,:password_confirmation,:remember_me,:vendor_attributes,:roles,:carrier_attributes

  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i , :message => "Please enter a valid email"
  validates_presence_of :email
  validates_presence_of :password,:password_confirmation, :on => :update, :if => Proc.new {|u| u.encrypted_password.blank?}
  validates :password, :length => {:within => Devise.password_length},:on => :update, :if => Proc.new {|u| u.encrypted_password.blank?}
  validates :password, :confirmation => true, :on => :update, :if => Proc.new {|u| u.encrypted_password.blank?}
  validates_presence_of :name, :on => :update
  validates :roles, :presence => true
  validates_uniqueness_of :email, :message => 'Email already used'

  has_one :vendor
  has_one :carrier
  has_many :managed_vendors, :foreign_key => :managed_by_id, :class_name => "Vendor"

  has_many :uploaded_files,:foreign_key => :uploaded_by_id, :class_name => "UploadedFile"

  has_many :invited_users,:class_name => "User", :foreign_key => :invited_by_id , :source_type => "User"

  accepts_nested_attributes_for :vendor,:carrier

  ROLES = %w(admin vendor vendor_manager external_integration carrier)
  AMAZON_ROLES = {"Admin" => "admin","Vendor Manager" => "vendor_manager" ,"External Integration" => "external_integration"}
  ALL_ROLES = {"Admin" => "admin","Vendor Manager" => "vendor_manager" ,"External Integration" => "external_integration", "Vendor" => "vendor","Carrier" => "carrier"}

  ROLES.each do |role|
    define_method "is_#{role}" do
      self.roles.include?(role)
    end
    define_method "#{role}?" do
      self.roles.include?(role)
    end
  end

  def roles
    (read_attribute(:roles) || '').strip.split(/\s+/)
  end

  def roles=(roles)
    write_attribute(:roles, (roles || ""))
  end

  def role_symbols
    roles.map do |role|
      role.underscore.to_sym
    end
  end

  def roles_by_user_role
    if self.admin?
      return ALL_ROLES
    elsif self.external_integration?
      return ALL_ROLES.except('Admin','External Integration')
    elsif self.vendor_manager?
      return ALL_ROLES.except('Admin','External Integration','Vendor Manager')
    else
      return []
    end
  end

  def invited_user?
    self.invitation_token.present? || self.invitation_accepted_at.present?
  end


  def has_admin_role?
    self.admin? || self.external_integration?
  end

end
