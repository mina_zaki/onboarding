class Vendor < ActiveRecord::Base

  attr_accessible :company_name, :payment_terms,:vendor_code, :answers_attributes,:managed_by_id,:street,:city,:postcode,:country,:carrier_comment

  validates_presence_of :vendor_code,:company_name

  belongs_to :user
  belongs_to :managed_by, :class_name => "User"

  has_many :uploaded_file_on_items,:as => :item
  has_many :uploaded_files,:through => :uploaded_file_on_items

  has_many :warehouses

  has_many :answers
  has_many :vendor_questions,:through => :answers,:order => "position ASC"

  has_many :contacts,:as => :item

  accepts_nested_attributes_for :answers


  default_scope where(:deleted_at => nil)

  after_create :create_answers

  def create_answers
    VendorQuestion.all.each do |vq|
      Answer.create({:vendor_question_id => vq.id,:vendor_id => self.id})
    end
  end

  def self.search(word)
    where("company_name LIKE ? or vendor_code LIKE ?","%#{word}%","%#{word}%")
  end

  def status
    if self.user.invitation_token.present?
      return "Invitation sent"
    elsif self.user.invitation_accepted_at.present?
      return "Registration Done"
    else
      return "Integration in Progress"
    end
  end

  def general_questions
    self.vendor_questions.general_information.includes(:answers).joins(:answers)
  end

  def logistics_questions
    self.vendor_questions.logistics.includes(:answers).joins(:answers)
  end

  self.per_page = 20

end
