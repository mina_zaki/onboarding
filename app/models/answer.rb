class Answer < ActiveRecord::Base
  belongs_to :vendor_question
  belongs_to :vendor

  before_update :format_multiple_answer

  attr_accessible :answer,:vendor_id,:vendor_question_id

  def format_multiple_answer
    if self.vendor_question.multiple_answer?
      self.answer = self[:answer].reject(&:blank?).join(",")
    end
  end

  def answer
    if self[:answer] and self.vendor_question.multiple_answer?
      self[:answer].split(',')
    end
  end

  def show_answer
    self[:answer]
  end

end
