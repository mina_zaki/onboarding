class Contact < ActiveRecord::Base
  belongs_to :item,:polymorphic => true
  attr_accessible :email, :name, :phone,:item_id,:item_type,:role

  validates_presence_of :name,:email
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i , :message => "Please enter a valid email"
end
