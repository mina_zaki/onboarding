class UsersController < ApplicationController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Users",:users_path

  def index
    permitted_to! :read, :users
    @users = User.page(params[:page])
  end

  def show
    @user = User.find(params[:id])
    permitted_to! :read, @user
    add_breadcrumb @user.name, :user_path
  end

  def edit
    @user = User.find(params[:id])
    permitted_to! :edit, @user
    add_breadcrumb @user.name, :user_path
    add_breadcrumb "Edit", :edit_user_path
  end

  def update
    @user = User.find(params[:id])
    permitted_to! :update, @user
    @user.update_attributes(update_params)
    if @user.save
      redirect_to user_path(@user.id)
    else
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    permitted_to! :delete,@user
    @user.destroy
    if @user.carrier? and @user.carrier.present?
      @user.carrier.update_attribute(:deleted_at,Time.now)
    elsif @user.vendor? and @user.vendor.present?
      @user.vendor.update_attribute(:deleted_at,Time.now)
    end
    redirect_to users_path
  end

  def update_params
    if current_user.admin?
      params.require(:user).permit(:email,:roles)
    else
      params.require(:user).permit(:email)
    end
  end

end
