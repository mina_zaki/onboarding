class InvitationMessagesController < ApplicationController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Invitation Messages",:invitation_messages_path

  def index
    permitted_to! :index , :invitation_messages
    @admin_message = InvitationMessage.find_by_message_type("admin")
    @external_integration_message = InvitationMessage.find_by_message_type("external_integration")
    @vendor_manager_message = InvitationMessage.find_by_message_type("vendor_manager")
    @vendor_message = InvitationMessage.find_by_message_type("vendor")
    @carrier_message = InvitationMessage.find_by_message_type("carrier")
  end

  def show
    @invitation_message = InvitationMessage.find(params[:id])
    permitted_to! :show,@invitation_message
  end

  def new
    @invitation_message = InvitationMessage.new(:message_type => params[:type])
    permitted_to! :new, @invitation_message
    add_breadcrumb "Add Invitation message",:new_invitation_message_path
  end

  def create
    @invitation_message = InvitationMessage.find_or_initialize_by_message_type(params[:invitation_message][:message_type])
    permitted_to! :create, @invitation_message
    @invitation_message.message = params[:invitation_message][:message]

    @invitation_message.save!

    redirect_to invitation_message_path(@invitation_message.id)

  end

  def edit
    @invitation_message = InvitationMessage.find(params[:id])
    permitted_to! :edit, @invitation_message
    add_breadcrumb "Edit",:edit_invitation_message_path
  end

  def update
    @invitation_message = InvitationMessage.find(params[:id])
    permitted_to! :update, @invitation_message
    @invitation_message.update_attribute(:message,params[:invitation_message][:message])
    redirect_to invitation_message_path(@invitation_message.id)
  end

end
