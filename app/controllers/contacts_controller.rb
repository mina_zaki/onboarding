class ContactsController < ApplicationController
  add_breadcrumb "Home", :root_path
  def new
    @contact = Contact.new
    @item = params[:item_type].constantize.find(params[:item_id])
    @contact.item = @item
    add_breadcrumb @contact.item_type + "s", Proc.new {|u| get_index_path(@item)}
    add_breadcrumb @item.company_name,Proc.new {|u| get_path(@item)}
    add_breadcrumb "New Contact",:new_contact_path
    permitted_to! :new, @contact
  end

  def create
    @contact = Contact.new(params[:contact])
    permitted_to! :create, @contact
    if @contact.save
      flash[:success]= "Contact created successfully"
      if @contact.item_type.eql?(Carrier.name)
        redirect_to carrier_path(@contact.item_id)
      elsif @contact.item_type.eql?(Vendor.name)
        redirect_to vendor_path(:id => @contact.item_id,:anchor => "contacts")
      end
    else
      render :new
    end
  end

  def edit
    @contact = Contact.find(params[:id])
    permitted_to! :edit, @contact
    add_breadcrumb @contact.item_type+ "s", Proc.new {|u| get_index_path(@contact.item)}
    add_breadcrumb @contact.item.company_name,Proc.new {|u| get_path(@contact.item)}
    add_breadcrumb "Edit Contact",:edit_contact_path
  end

  def update
    @contact = Contact.find(params[:id])
    permitted_to! :update, @contact
    if @contact.update_attributes(params[:contact])
      flash[:success]= "Contact updated successfully"
      if @contact.item_type.eql?(Carrier.name)
        redirect_to carrier_path(@contact.item_id)
      elsif @contact.item_type.eql?(Vendor.name)
        redirect_to vendor_path(:id => @contact.item_id,:anchor => "contacts")
      end
    else
      render :edit
    end
  end

  def destroy
    @contact = Contact.find(params[:id])
    permitted_to! :delete,@contact
    @item_type = @contact.item_type
    @item_id = @contact.item_id
    @contact.destroy
    flash[:success]= "Contact removed successfully"
    if @item_type.eql?(Carrier.name)
      redirect_to carrier_path(:id => @item_id)
    elsif @item_type.eql?(Vendor.name)
      redirect_to vendor_path(:id => @item_id,:anchor => "contacts")
    end
  end

  private

  def get_path(item)
    if item.is_a?(Vendor)
      return vendor_path(:id => item.id)
    else
      return carrier_path(:id => item.id)
    end
  end
  def get_index_path(item)
    if item.is_a?(Vendor)
      return vendors_path
    else
      return carriers_path
    end
  end
end
