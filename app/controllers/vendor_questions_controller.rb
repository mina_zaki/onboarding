class VendorQuestionsController < ApplicationController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Vendor Questions",:vendor_questions_path

  def index
    permitted_to! :read, :vendor_questions
    @general_information = VendorQuestion.general_information
    @logistics = VendorQuestion.logistics
    flash.now[:notice] = "You can drag and drop the questions to sort them!"
  end

  def new
    permitted_to! :new, :vendor_question
    @vendor_question =  VendorQuestion.new
    add_breadcrumb "New Vendor Question", :new_vendor_question_path
  end

  def create
    permitted_to! :create, :vendor_question
    @vendor_question = VendorQuestion.new(params[:vendor_question])
    if @vendor_question.save
      flash[:success] = "Vendor Question created successfully"
      redirect_to vendor_questions_path
    else
      render :new
    end

  end

  def edit
    @vendor_question = VendorQuestion.find(params[:id])
    permitted_to! :edit, @vendor_question
    add_breadcrumb @vendor_question.question, :edit_vendor_question_path

  end

  def update
    @vendor_question = VendorQuestion.find(params[:id])
    permitted_to! :update, @vendor_question
    if @vendor_question.update_attributes(params[:vendor_question])
      flash[:success] = "Vendor Question updated successfully"
      redirect_to vendor_questions_path
    else
      render :edit
    end
  end

  def sort
    @vendor_question = VendorQuestion.find(params[:id])
    @vendor_question.update_attribute(:position,params[:position])
    render :json => "success"
  end

  def destroy
    @vendor_question = VendorQuestion.find(params[:id])
    @vendor_question.destroy
    flash[:success] = "Vendor Question deleted successfully"
    redirect_to vendor_questions_path
  end

end
