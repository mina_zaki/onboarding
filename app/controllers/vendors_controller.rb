require 'csv'
class VendorsController < ApplicationController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Vendors",:vendors_path, :if => Proc.new {|p| permitted_to? :index,:vendors}
  before_filter :authenticate_user!

  layout 'application'

  def index
    permitted_to! :index, :vendors
    respond_to do |format|
      format.html do
        @vendors = Vendor.page(params[:page])
      end
      format.csv do
        @vendors = Vendor.find(params[:ids])
        headers['Content-Disposition'] = "attachment; filename=\"vendor-list\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end

  def edit
    @vendor = Vendor.find(params[:id])
    permitted_to! :edit, @vendor
    add_breadcrumb @vendor.company_name,:vendor_path
    add_breadcrumb "Edit",:edit_vendor_path
  end

  def update
    @vendor = Vendor.find(params[:id])
    if current_user.carrier?
      permitted_to! :post_comment_to_vendor, @vendor
      @params = update_carrier_comments
    else
      permitted_to! :update, @vendor
      @params = params[:vendor]
    end

    respond_to do |format|
      if @vendor.update_attributes(@params)
        format.html do
          flash[:success] = "Vendor updated successfully"
          redirect_to vendor_path(@vendor.id)
        end
        format.json do
          render :json => true
        end
      else
        format.html do
          render :edit
        end
        format.json do
          render :json => "error"
        end
      end
    end
  end

  def show
    if params[:id].present?
      @vendor = Vendor.find(params[:id])
    else
      @vendor = current_user.vendor
    end
    permitted_to! :show, @vendor
    add_breadcrumb @vendor.company_name,:vendor_path
    @general_questions = @vendor.general_questions
    @logistics_questions = @vendor.logistics_questions
    @vendor_files = UploadedFile.vendor_files
    respond_to do |format|
      format.html
      format.xls do
        headers['Content-Disposition'] = "attachment; filename=\"#{@vendor.company_name}.xls\""
        headers['Content-Type'] ||= 'application/xls'
      end
    end

  end

  def edit_questionnaire
    @vendor = Vendor.find(params[:vendor_id])
    @section = VendorQuestion::SECTIONS[params[:section]]
    @answers = @vendor.answers.includes(:vendor_question).joins(:vendor_question).where("vendor_questions.section in (?)", @section).order("position ASC")
    permitted_to! :edit, @vendor
    add_breadcrumb @vendor.company_name,Proc.new {|c| vendor_path(:id => @vendor.id)}
    add_breadcrumb params[:section],:vendor_edit_questionnaire_path
  end

  def search
    permitted_to! :index, :vendors
    @search = params[:vendor][:word]
    @vendors = Vendor.search(@search).page(params[:page])
    render :index
  end

  def update_questionnaire
    @vendor = Vendor.find(params[:vendor_id])
    @section = params[:section]
    permitted_to! :update, @vendor
    if @vendor.update_attributes(params[:vendor])
      redirect_to vendor_path(@vendor,:anchor => @section)
    else
      render :edit_questionnaire
    end
  end

  def destroy
    @vendor = Vendor.find(params[:id])
    permitted_to! :destroy, @vendor
    @vendor.update_attribute(:deleted_at,Time.now)
    @vendor.user.destroy
    flash[:success] = "Vendor removed successfully"
    redirect_to vendors_path
  end

  def update_carrier_comments
    params.require(:vendor).permit(:carrier_comment)
  end

end


