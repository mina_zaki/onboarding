class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_in_path_for(resource)
    if resource.vendor? and resource.vendor.present?
      flash[:success] = t('vendor_welcome_notification')
      return vendor_path(:id => resource.vendor.id)
    elsif resource.carrier? and resource.carrier.present?
      return vendors_path
    else
      request.env['omniauth.origin'] || stored_location_for(resource) || root_path
    end

  end

end
