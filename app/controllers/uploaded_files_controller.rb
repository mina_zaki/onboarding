class UploadedFilesController < ApplicationController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Uploaded files",:uploaded_files_path

  def new
    add_breadcrumb "Upload files",:new_uploaded_file_path
  end

  def index
    @vendor_files =  UploadedFile.vendor_files
  end

  def create
    unless params[:chunk].present?
      @uploaded_file=UploadedFile.new(:blob => params[:file])
      @uploaded_file.uploaded_by = current_user
      @uploaded_file.is_for_vendor =  params[:is_for_vendor]
      @uploaded_file.save
      flash[:success] = "File uploaded successfully"
      if params[:item_id].present? and params[:item_type].present?
        @uploaded_file_on_item = UploadedFileOnItem.new
        @uploaded_file_on_item.uploaded_file = @uploaded_file
        @uploaded_file_on_item.item_id = params[:item_id]
        @uploaded_file_on_item.item_type = params[:item_type]
        @uploaded_file_on_item.save
        redirect_to vendor_path(:id => @uploaded_file_on_item.item_id,:anchor => "uploaded_files") and return
      end
      redirect_to uploaded_files_path and return
    else
      chunk = params[:chunk]
      chunks = params[:chunks]
      # filename = params[:name]
      path = File.join(Rails.root,"tmp",File.basename(params[:name]))
      # upload_path = "#{Rails.root}/tmp"
      mode = (chunk == "0") ? "wb" : "ab"
      io = params[:file]
      File.open(path, mode) do |file|
        file.write(io.read)
      end
      if (chunk.to_i == (chunks.to_i - 1))
        file = File.new(path, "r")
        @uploaded_file=UploadedFile.new(:blob => file)
        @uploaded_file.uploaded_by = current_user
        @uploaded_file.is_for_vendor =  params[:is_for_vendor]
        @uploaded_file.save
        flash[:success] = "File uploaded successfully"
        File.delete(path)
        if params[:item_id].present? and params[:item_type].present?
          @uploaded_file_on_item = UploadedFileOnItem.new
          @uploaded_file_on_item.uploaded_file = @uploaded_file
          @uploaded_file_on_item.item_id = params[:item_id]
          @uploaded_file_on_item.item_type = params[:item_type]
          @uploaded_file_on_item.save
          redirect_to vendor_path(:id => @uploaded_file_on_item.item_id,:anchor => "uploaded_files") and return
        end
        redirect_to uploaded_files_path and return
      else
        render :json => "chunk uploaded"
      end
    end
  end

  def destroy
    @uploaded_file = UploadedFile.find(params[:id])
    permitted_to! :delete, @uploaded_file
    @uploaded_file.update_attribute(:deleted_at,Time.now)
    flash[:success] = "File removed successfully"
    if @uploaded_file.uploaded_file_on_item.present?
      redirect_to vendor_path(:id => @uploaded_file.uploaded_file_on_item.item_id,:anchor => "uploaded_files")
    else
      redirect_to uploaded_files_path
    end
  end

end
