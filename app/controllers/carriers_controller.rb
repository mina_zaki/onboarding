class CarriersController < ApplicationController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Carriers",:carriers_path
  def index
    permitted_to! :index, :carriers
    @carriers = Carrier.page(params[:page])
  end

  def show
    if params[:id].present?
      @carrier = Carrier.find(params[:id])
    else
      @carrier = current_user.carrier
    end
    permitted_to! :show, @carrier
    add_breadcrumb @carrier.company_name,:carrier_path
  end

  def edit
    @carrier = Carrier.find(params[:id])
    permitted_to! :edit, @carrier
    add_breadcrumb @carrier.company_name,:carrier_path
    add_breadcrumb "Edit",:edit_carrier_path
  end

  def update
    @carrier = Carrier.find(params[:id])
    permitted_to! :update, @carrier
    if @carrier.update_attributes(params[:carrier])
      flash[:success] = "Contact updated successfully"
      redirect_to carrier_path(@carrier.id)
    else
      render :edit
    end
  end
  def destroy
    @carrier = Carrier.find(params[:id])
    permitted_to! :delete, @carrier
    @carrier.update_attribute(:deleted_at,Time.now)
    @carrier.user.destroy
    flash[:success] = "Carrier removed successfully"
    redirect_to carriers_path
  end
end
