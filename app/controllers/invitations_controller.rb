class InvitationsController < Devise::InvitationsController
  before_filter :authenticate_user!, :only => [:new,:create]
  before_filter :update_params, if: :devise_controller? ,:only => :update
  add_breadcrumb "Home",:root_path
  def new
    permitted_to! :invite,:users
    @user = User.new
    if params[:role].eql?("vendor")
      @user.vendor = Vendor.new
      @user.roles = "vendor"
      @invitation_message = InvitationMessage.find_by_message_type("vendor")
      @invitation_message = @invitation_message ? @invitation_message.message : @invitation_message
      add_breadcrumb "Invite vendor",:new_user_invitation_path
      render :new_vendor
    elsif params[:role].eql?("carrier")
      @user.carrier = Carrier.new
      @user.roles = "carrier"
      @invitation_message = InvitationMessage.find_by_message_type("carrier")
      @invitation_message = @invitation_message ? @invitation_message.message : @invitation_message
      add_breadcrumb "Invite carrier",:new_user_invitation_path
      render :new_carrier
    else
      @invitation_messages = InvitationMessage.all
      add_breadcrumb "Invite admin",:new_user_invitation_path
    end
  end

  def create
    @user = User.new(params[:user])
    permitted_to! :invite,@user
    if @user.valid?
      @message = params[:message]
      @user =  User.invite!(params[:user],current_user) do |u|
        u.skip_invitation = true
        u.invitation_sent_at = Time.now
      end
      UserMailer.invite_message(@user, @message).deliver
      # @user.deliver_invitation # mark invitation as delivered
      flash[:success] = "User invited successfully"
      redirect_to root_path
      else
        if @user.vendor?
          @invitation_message = InvitationMessage.find_by_message_type("vendor")
          @invitation_message = params[:message].present? ? params[:message] : @invitation_message.message
          render :new_vendor
        elsif @user.carrier
          @invitation_message = InvitationMessage.find_by_message_type("carrier")
          @invitation_message = params[:message].present? ? params[:message] : @invitation_message.message
          render :new_carrier
        else
          @message = params[:message]
          @invitation_messages = InvitationMessage.all
          render :new
        end

      end

  end

  def update
    self.resource = accept_resource

    if resource.errors.empty?
      yield resource if block_given?
      flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
      set_flash_message :notice, flash_message
      sign_in(resource_name, resource)
      if resource.vendor?
        flash[:success] = t("vendor_welcome_notification")
        redirect_to vendor_path(:id => resource.vendor.id)
      else
        respond_with resource, :location => after_accept_path_for(resource)
      end
    else
      respond_with_navigational(resource){ render :edit }
    end
  end

  protected

  def update_params
    devise_parameter_sanitizer.for(:accept_invitation) do |u|
      u.permit(:name,:password,:password_confirmation,:invitation_token)
    end
  end

  def create_params
    devise_parameter_sanitizer.for(:invite) do |u|
      u.permit(:email,:roles,:managed_by_id,:vendor => [:company_name,:vendor_code,:payment_terms])
    end
  end

end