class WarehousesController < ApplicationController

  add_breadcrumb "Home", :root_path
  add_breadcrumb "Vendors",:vendors_path

  def new
    @warehouse = Warehouse.new
    @vendor = Vendor.find(params[:vendor_id])
    @warehouse.vendor = @vendor
    permitted_to! :create, @warehouse
    add_breadcrumb @vendor.company_name, Proc.new {|c| vendor_path(:id => @vendor.id)}
    add_breadcrumb "Add warehouse",:new_warehouse_path
  end

  def create
    @warehouse = Warehouse.new(params[:warehouse])
    @vendor = Vendor.find(params[:warehouse][:vendor_id])
    @warehouse.vendor = @vendor
    permitted_to! :create, @warehouse
    if @warehouse.save
      flash[:success] = "Warehouse created successfully"
      redirect_to vendor_path(:id => @warehouse.vendor_id,:anchor => "warehouses")
    else
      render :new
    end
  end

  def edit
    @warehouse = Warehouse.find(params[:id])
    @vendor = @warehouse.vendor
    permitted_to! :edit, @warehouse
    add_breadcrumb @vendor.company_name, Proc.new {|c| vendor_path(:id => @vendor.id)}
    add_breadcrumb @warehouse.name,:edit_warehouse_path
  end

  def update
    @warehouse = Warehouse.find(params[:id])
    permitted_to! :update, @warehouse
    if @warehouse.update_attributes(params[:warehouse])
      flash[:success] = "Warehouse updated successfully"
      redirect_to vendor_path(:id => @warehouse.vendor_id,:anchor => "warehouses")
    else
      render :edit
    end
  end
  def destroy
    @warehouse = Warehouse.find(params[:id])
    permitted_to! :delete, @warehouse
    @warehouse.update_attribute(:deleted_at,Time.now)
    flash[:success] = "Warehouse removed successfully"
    redirect_to vendor_path(:id => @warehouse.vendor_id,:anchor => "warehouses")
  end
end
