class CreateInvitationMessages < ActiveRecord::Migration
  def change
    create_table :invitation_messages do |t|
      t.text :message
      t.string :type

      t.timestamps
    end
  end
end
