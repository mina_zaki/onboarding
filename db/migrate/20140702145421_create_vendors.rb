class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.references :user
      t.string :company_name
      t.string :vendor_code
      t.string :account_iban
      t.string :account_name
      t.string :product_line
      t.integer :units_shipped

      t.timestamps
    end
    add_index :vendors, :user_id
  end
end
