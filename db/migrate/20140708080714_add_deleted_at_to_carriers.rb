class AddDeletedAtToCarriers < ActiveRecord::Migration
  def change
    add_column :carriers, :deleted_at, :datetime
  end
end
