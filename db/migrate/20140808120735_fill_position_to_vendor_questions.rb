class FillPositionToVendorQuestions < ActiveRecord::Migration
  def up
    counter = 1
    VendorQuestion.general_information.each do |vq|
      vq.update_attribute(:position,counter)
      counter = counter + 1
    end
    counter = 1
    VendorQuestion.logistics.each do |vq|
      vq.update_attribute(:position,counter)
      counter = counter + 1
    end
  end

  def down
  end
end
