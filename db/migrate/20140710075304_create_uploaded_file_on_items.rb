class CreateUploadedFileOnItems < ActiveRecord::Migration
  def change
    create_table :uploaded_file_on_items do |t|
      t.references :item, :polymorphic => true
      t.references :uploaded_file

      t.timestamps
    end
    add_index :uploaded_file_on_items, :item_id
    add_index :uploaded_file_on_items, :uploaded_file_id
  end
end
