class RemovePhonesFromUsers < ActiveRecord::Migration
  def up
    remove_column :users,:phone_1
    remove_column :users,:phone_2
  end

  def down
  end
end
