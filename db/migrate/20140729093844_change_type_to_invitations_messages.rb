class ChangeTypeToInvitationsMessages < ActiveRecord::Migration
  def up
    rename_column :invitation_messages,:type,:message_type
  end

  def down
  end
end
