class AddPositionToVendorQuestions < ActiveRecord::Migration
  def change
    add_column :vendor_questions, :position, :integer
  end
end
