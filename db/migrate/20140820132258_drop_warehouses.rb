class DropWarehouses < ActiveRecord::Migration
  def up
    drop_table :warehouses
  end

  def down
  end
end
