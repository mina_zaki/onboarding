class CreateWarehouses < ActiveRecord::Migration
  def change
    create_table :warehouses do |t|
      t.string :name
      t.string :street
      t.string :postcode
      t.string :city
      t.string :country
      t.string :phone
      t.references :vendor
      t.timestamps
    end
  end
end
