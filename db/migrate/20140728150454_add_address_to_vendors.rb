class AddAddressToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :street, :string
    add_column :vendors, :postcode, :string
    add_column :vendors, :city, :string
    add_column :vendors, :country, :string
  end
end
