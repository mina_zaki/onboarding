class AddDeletedAtToWarehouses < ActiveRecord::Migration
  def change
    add_column :warehouses, :deleted_at, :datetime
  end
end
