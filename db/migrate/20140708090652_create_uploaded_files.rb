class CreateUploadedFiles < ActiveRecord::Migration
  def change
    create_table :uploaded_files do |t|
      t.timestamps
    end
    add_attachment :uploaded_files, :blob
  end
end
