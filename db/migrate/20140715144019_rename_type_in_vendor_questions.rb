class RenameTypeInVendorQuestions < ActiveRecord::Migration
  def up
    rename_column :vendor_questions, :type, :question_type
  end

  def down
  end
end
