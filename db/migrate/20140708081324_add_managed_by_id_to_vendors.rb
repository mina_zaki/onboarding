class AddManagedByIdToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :managed_by_id, :integer

    add_foreign_key :vendors, :users,:column => :managed_by_id
  end
end
