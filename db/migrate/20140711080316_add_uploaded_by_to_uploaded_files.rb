class AddUploadedByToUploadedFiles < ActiveRecord::Migration
  def change
    add_column :uploaded_files, :uploaded_by_id, :integer
    add_foreign_key :uploaded_files, :users, :column => :uploaded_by_id
  end
end
