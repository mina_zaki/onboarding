class CreateUploadedFileOnUsers < ActiveRecord::Migration
  def change
    create_table :uploaded_file_on_users do |t|
      t.references :uploaded_file
      t.references :user
      t.timestamps
    end
  end
end
