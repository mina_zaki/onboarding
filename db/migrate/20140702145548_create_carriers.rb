class CreateCarriers < ActiveRecord::Migration
  def change
    create_table :carriers do |t|
      t.references :user
      t.string :company_name

      t.timestamps
    end
    add_index :carriers, :user_id
  end
end
