class CreateAnswersForVendors < ActiveRecord::Migration
  def up
    Vendor.all.each do |vendor|
      VendorQuestion.all.each do |vq|
        Answer.create({:vendor_id => vendor.id,:vendor_question_id => vq.id})
      end
    end
  end

  def down
  end
end
