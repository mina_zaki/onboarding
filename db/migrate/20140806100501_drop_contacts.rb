class DropContacts < ActiveRecord::Migration
  def up
    if Contact.table_exists?
      drop_table :contacts
    end
  end

  def down
  end
end
