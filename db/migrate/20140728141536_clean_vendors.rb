class CleanVendors < ActiveRecord::Migration
  def up
    remove_column :vendors, :account_iban
    remove_column :vendors, :account_name
    remove_column :vendors, :product_line
    remove_column :vendors, :units_shipped
    add_column :vendors, :payment_terms,:string
  end

  def down
  end
end
