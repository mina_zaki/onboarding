class AddCarrierCommentToVendor < ActiveRecord::Migration
  def change
    add_column :vendors, :carrier_comment, :string
  end
end
