class AddDetailsToWarehouses < ActiveRecord::Migration
  def change
    add_column :warehouses, :method_of_loading, :string
    add_column :warehouses, :is_truck_40t, :boolean
    add_column :warehouses, :is_truck_14t, :boolean
    add_column :warehouses, :is_truck_7_5t, :boolean
    add_column :warehouses, :is_transporter, :boolean
    add_column :warehouses, :is_lifting_ramp_required, :boolean
    add_column :warehouses, :is_forklifts_in_place, :boolean
    add_column :warehouses, :is_in_pedestrian_area, :boolean
    add_column :warehouses, :is_in_traffic_calmed_area, :boolean
    add_column :warehouses, :is_accessible_on_ground_level, :boolean
    add_column :warehouses, :height_of_ramps, :decimal, :precision => 5, :scale => 5
  end
end
