class CreateVendorQuestions < ActiveRecord::Migration
  def change
    create_table :vendor_questions do |t|
      t.string :type
      t.string :options
      t.string :section
      t.text :question

      t.timestamps
    end

  end
end
