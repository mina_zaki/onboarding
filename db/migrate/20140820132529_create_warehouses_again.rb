class CreateWarehousesAgain < ActiveRecord::Migration
  def up
    create_table :warehouses do |t|
      t.string :name
      t.string :street
      t.string :postcode
      t.string :city
      t.string :country
      t.string :phone
      t.references :vendor
      t.string :ship_label_generation
      t.string :pickup_trucks
      t.string :remarks
      t.decimal :height_of_ramps ,:precision => 6, :scale => 4
      t.timestamps
    end
  end

  def down
  end
end
