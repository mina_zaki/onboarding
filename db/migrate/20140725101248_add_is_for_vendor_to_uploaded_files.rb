class AddIsForVendorToUploadedFiles < ActiveRecord::Migration
  def change
    add_column :uploaded_files, :is_for_vendor, :boolean
  end
end
