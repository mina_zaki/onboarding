class AddDeletedAtToUploadedFiles < ActiveRecord::Migration
  def change
    add_column :uploaded_files, :deleted_at, :datetime
  end
end
