class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.references :item,:polymorphic => true

      t.timestamps
    end
    add_index :contacts, :item_id
  end
end
