class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.text :answer
      t.references :vendor_question
      t.references :vendor

      t.timestamps
    end
    add_index :answers, :vendor_question_id
    add_index :answers, :vendor_id
  end
end
