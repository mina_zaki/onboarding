# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140820133027) do

  create_table "answers", :force => true do |t|
    t.text     "answer"
    t.integer  "vendor_question_id"
    t.integer  "vendor_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "answers", ["vendor_id"], :name => "index_answers_on_vendor_id"
  add_index "answers", ["vendor_question_id"], :name => "index_answers_on_question_id"

  create_table "carriers", :force => true do |t|
    t.integer  "user_id"
    t.string   "company_name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.datetime "deleted_at"
  end

  add_index "carriers", ["user_id"], :name => "index_carriers_on_user_id"

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.integer  "item_id"
    t.string   "item_type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "role"
  end

  add_index "contacts", ["item_id"], :name => "index_contacts_on_item_id"

  create_table "invitation_messages", :force => true do |t|
    t.text     "message"
    t.string   "message_type"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "questionnaire_answers", :force => true do |t|
    t.integer  "user_on_questionnaire_id"
    t.integer  "question_id"
    t.text     "answer"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "questionnaire_answers", ["question_id"], :name => "index_questionnaire_answers_on_question_id"
  add_index "questionnaire_answers", ["user_on_questionnaire_id"], :name => "index_questionnaire_answers_on_user_on_questionnaire_id"

  create_table "questionnaires", :force => true do |t|
    t.string   "name"
    t.integer  "created_by_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "questionnaires", ["created_by_id"], :name => "questionnaires_created_by_id_fk"

  create_table "questions", :force => true do |t|
    t.text     "question"
    t.boolean  "is_required"
    t.integer  "questionnaire_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "questions", ["questionnaire_id"], :name => "index_questions_on_questionnaire_id"

  create_table "uploaded_file_on_items", :force => true do |t|
    t.integer  "item_id"
    t.string   "item_type"
    t.integer  "uploaded_file_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "uploaded_file_on_items", ["item_id"], :name => "index_uploaded_file_on_items_on_item_id"
  add_index "uploaded_file_on_items", ["uploaded_file_id"], :name => "index_uploaded_file_on_items_on_uploaded_file_id"

  create_table "uploaded_files", :force => true do |t|
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "blob_file_name"
    t.string   "blob_content_type"
    t.integer  "blob_file_size"
    t.datetime "blob_updated_at"
    t.integer  "uploaded_by_id"
    t.boolean  "is_for_vendor"
    t.datetime "deleted_at"
  end

  add_index "uploaded_files", ["uploaded_by_id"], :name => "uploaded_files_uploaded_by_id_fk"

  create_table "user_on_questionnaires", :force => true do |t|
    t.integer  "user_id"
    t.integer  "questionnaire_id"
    t.boolean  "is_finished"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "user_on_questionnaires", ["questionnaire_id"], :name => "index_user_on_questionnaires_on_questionnaire_id"
  add_index "user_on_questionnaires", ["user_id"], :name => "index_user_on_questionnaires_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
    t.string   "phone"
    t.string   "password_salt"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      :default => 0
    t.string   "roles"
    t.datetime "deactivated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token", :unique => true
  add_index "users", ["invitations_count"], :name => "index_users_on_invitations_count"
  add_index "users", ["invited_by_id"], :name => "index_users_on_invited_by_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "vendor_questions", :force => true do |t|
    t.string   "question_type"
    t.string   "options"
    t.string   "section"
    t.text     "question"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "position"
  end

  create_table "vendors", :force => true do |t|
    t.integer  "user_id"
    t.string   "company_name"
    t.string   "vendor_code"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.datetime "deleted_at"
    t.integer  "managed_by_id"
    t.string   "payment_terms"
    t.string   "street"
    t.string   "postcode"
    t.string   "city"
    t.string   "country"
    t.string   "carrier_comment"
    t.datetime "integration_finished_at"
  end

  add_index "vendors", ["managed_by_id"], :name => "vendors_managed_by_id_fk"
  add_index "vendors", ["user_id"], :name => "index_vendors_on_user_id"

  create_table "warehouses", :force => true do |t|
    t.string   "name"
    t.string   "street"
    t.string   "postcode"
    t.string   "city"
    t.string   "country"
    t.string   "phone"
    t.integer  "vendor_id"
    t.string   "ship_label_generation"
    t.string   "pickup_trucks"
    t.string   "remarks"
    t.decimal  "height_of_ramps",       :precision => 6, :scale => 4
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.datetime "deleted_at"
  end

  add_foreign_key "questionnaires", "users", name: "questionnaires_created_by_id_fk", column: "created_by_id"

  add_foreign_key "uploaded_files", "users", name: "uploaded_files_uploaded_by_id_fk", column: "uploaded_by_id"

  add_foreign_key "vendors", "users", name: "vendors_managed_by_id_fk", column: "managed_by_id"

end
