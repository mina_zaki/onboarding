require 'capistrano/ext/multistage'
require "capistrano-rbenv"


set :default_stage, "production"
set :application, "onboarding"

set :user, "ubuntu"
set :deploy_to, "/var/apps/www/onboarding-tool"
set :use_sudo, false

role :web, "54.77.160.89"                          # Your HTTP server, Apache/etc
role :app, "54.77.160.89"                          # This may be the same as your `Web` server
role :db,  "54.77.160.89", :primary => true # This is where Rails migrations will run


#git configurations
set :repository,  "git@bitbucket.org:mina_zaki/onboarding.git"
set :scm, :git
ssh_options[:keys] = ["~/.ssh/gsg-keypair.pem"]
default_run_options[:pty] = true
ssh_options[:forward_agent] = true
ssh_options[:auth_methods] = ["publickey"]


# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# namespace :bundle do
#   desc "run bundle install and ensure all gem requirements are met"
#   task :install do
#     run "cd #{current_path} && bundle install  --without=test --no-update-sources"
#   end
# end



namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

after "deploy", "deploy:restart"
# after "deploy", "bundle:install"