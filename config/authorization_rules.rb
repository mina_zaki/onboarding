authorization do
  #super admin
  role :admin do
    has_omnipotence #permission on everything
  end

  role :external_integration do
    has_permission_on :users, :to => [:manage,:invite] do
      if_attribute :roles => intersects_with{['vendor_manager','carrier','vendor','external_integration']}
    end
    has_permission_on :vendors, :to => [:manage]
    has_permission_on :warehouses, :to => [:manage]
    has_permission_on :carriers ,:to => [:manage]
    has_permission_on :invitation_messages, :to => [:manage]
    has_permission_on :uploaded_files,:to => [:manage]
    has_permission_on :contacts,:to => [:manage]
    has_permission_on :vendor_questions,:to => [:manage]
  end

  role :vendor_manager do
    has_permission_on :users, :to => [:manage,:invite] do
      if_attribute :roles => intersects_with{['carrier','vendor']}
    end
    has_permission_on :vendors, :to => [:manage]
    has_permission_on :warehouses, :to => [:manage]
    has_permission_on :carriers ,:to => [:manage]
    has_permission_on :invitation_messages, :to => [:manage]
    has_permission_on :uploaded_files,:to => [:manage]
    has_permission_on :contacts,:to => [:manage]
    has_permission_on :vendor_questions,:to => [:manage]
  end

  role :vendor do
    has_permission_on :vendors, :to => [:update,:show] do
      if_attribute :user_id => is{user.id}
    end
    has_permission_on :warehouses, :to => [:manage] do
      if_attribute :vendor_id => is{user.vendor.id}
    end
    has_permission_on :uploaded_files,:to => [:manage] do
      if_attribute :uploaded_by_id => is {user.id}
    end
    has_permission_on :contacts,:to => [:manage],:join_by => :and  do
      if_attribute :item_type => is{Vendor.name}
      if_attribute :item_id => is{user.vendor.id}
    end
  end

  role :carrier do
    has_permission_on :vendors, :to => [:read,:post_comment_to_vendor]
    has_permission_on :carriers, :to => [:create,:update,:show] do
      if_attribute :user_id => is{user.id}
    end
    has_permission_on :uploaded_files,:to => [:manage] do
      if_attribute :uploaded_by_id => is {user.id}
    end
    has_permission_on :contacts,:to => [:manage],:join_by => :and  do
      if_attribute :item_type => is{Carrier.name}
      if_attribute :item_id => is{user.carrier.id}
    end
  end

end
privileges do
  privilege :manage,  :includes => [:create, :read, :update, :delete]
  privilege :read,    :includes => [:index, :show]
  privilege :create,  :includes => :new
  privilege :update,  :includes => :edit
  privilege :delete,  :includes => :destroy
  privilege :invite
  privilege :post_comment_to_vendor
end

